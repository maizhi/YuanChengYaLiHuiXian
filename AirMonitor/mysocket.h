#pragma once

#include <QTcpSocket>
#include "mymqtt.h"

class MySocket : public QTcpSocket
{
	Q_OBJECT

public:
	MySocket(QObject *parent, QTcpSocket* socket);
	~MySocket();


private:
	QTcpSocket* _socket = nullptr;
	QByteArray data;

	MyMqtt *mqtt = nullptr;

signals:
	void conn_unconnected();
//
//public slots:
//	void stateChanged(QAbstractSocket::SocketState);
//	void ready_read();
};
