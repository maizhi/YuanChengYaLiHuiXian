#pragma once

#include <QWidget>
#include <QtMqtt>
#include <QQueue>
#include <QJsonObject>

class MyMqtt : public QObject
{
	Q_OBJECT

public:
	MyMqtt(QObject *parent);
	~MyMqtt();
	void m_publish(QJsonObject);

private:
	QMqttClient* client = nullptr;
	QQueue<int> queue_message;  // ��Ϣ����

	bool is_connected = false;

};
