#include "airmonitor.h"
#include <QSettings>

AirMonitor::AirMonitor(QWidget* parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	// custom
	//sensor = new Sensor(this);

	// 关联
	//connect(sensor, &Sensor::connected, [=]() {


	//

	lbl_msg = new QLabel();
	ui.statusBar->addWidget(lbl_msg, true);

	server = new MyServer(this);

	connect(ui.btnTest, &QPushButton::clicked, this, [=] {
		server->test();
	});

	connect(server, &MyServer::message, [=](QString msg) {
		lbl_msg->setText(msg);
	});

	connect(server, &MyServer::update_pressure, [=](QJsonObject obj) {
		QString device = obj["device"].toString();
		int pressure = obj["pressure"].toInt();

		if (device.compare("Line1") == 0) {
			ui.pressure_1->setText(QString::number(pressure));
		}
		if (device == "Line2") {
			ui.pressure_2->setText(QString::number(pressure));
		}
	});

	// 更新连接数
	connect(server, &MyServer::update_conn, [=](int num) {

		ui.num_of_conn->setText(QString::number(num));

	});


	// 显示监听信息
	QSettings setting("config.ini", QSettings::IniFormat);
	QString server_address = setting.value("server", "127.0.0.1").toString();
	int server_port = setting.value("port", 9000).toInt();

	ui.lbl_listen->setText(QString("listen at %1:%2").arg(server_address).arg(server_port));
}
