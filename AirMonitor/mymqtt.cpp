#include "mymqtt.h"
#include <QTimer>
#include <QJsonDocument>

MyMqtt::MyMqtt(QObject* parent)
	: QObject(parent)
{
	client = new QMqttClient(this);
	client->setHostname("192.168.1.100");
	client->setPort(1883);
	client->connectToHost();

	// 失去连接
	connect(client, &QMqttClient::disconnected, [=]() {
		is_connected = false;

		QTimer::singleShot(10000, [=]() {
			if (client != nullptr)
				client->connectToHost();
		});
	});

	// 连接上
	connect(client, &QMqttClient::connected, [=]() {
		is_connected = true;
	});

}

MyMqtt::~MyMqtt()
{
}

/*发布信息*/
void MyMqtt::m_publish(QJsonObject obj) {

	QMqttTopicName topic("/pressure/");
	QJsonDocument documnet;
	documnet.setObject(obj);
	QByteArray msg = documnet.toJson();

	if (is_connected && client != nullptr) {
		client->publish(topic, msg);
	}

}
