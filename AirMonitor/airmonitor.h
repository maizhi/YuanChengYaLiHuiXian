#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_airmonitor.h"
#include "sensor.h"
#include "myserver.h"

class AirMonitor : public QMainWindow
{
	Q_OBJECT

public:
	AirMonitor(QWidget *parent = Q_NULLPTR);

private:
	Ui::AirMonitorClass ui;
	Sensor *sensor = nullptr;

	MyServer* server = nullptr;

	QLabel* lbl_msg = nullptr;
};
