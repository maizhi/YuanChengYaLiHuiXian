#pragma once

#include <QObject>
#include <QTcpServer>
#include "mymqtt.h"
#include <QJsonObject>

#define MERGE(a,b) (a<<8 | b)

//#pragma pack(1)
struct Data {
	quint8 id_slave;  // վ��
	quint8 fc;   // ���ܺ�
	quint8 add_h;  // д�Ĵ�����ַ
	quint8 add_l;

	quint8 register_h; // д�Ĵ����� 3
	quint8 register_l;
	quint8 bytes;  // д�ֽ��� 6

	quint8 d0_h;  // ����
	quint8 d0_l;
	quint8 d1_h;
	quint8 d1_l;
	quint8 d2_h;
	quint8 d2_l;

	quint8 crc1; // CRC
	quint8 crc2;
};

struct CRC {
	char crc1;
	char crc2;
};

class MyServer : public QObject
{
	Q_OBJECT

public:
	MyServer(QObject* parent);
	~MyServer();
	void test();

private:
	QTcpServer* server = nullptr;
	int id_socket = 0;

	QList<QTcpSocket*> _sockets;
	CRC MyServer::crc16(const QByteArray&);

	MyMqtt* mqtt = nullptr;

	QMap<int, QString> device_name;

public slots:
	void stateChanged(QAbstractSocket::SocketState);
	void ready_read();

signals:
	void message(QString);
signals:
	void update_pressure(QJsonObject);
signals:
	void update_conn(int);
};
