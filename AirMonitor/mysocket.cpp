#include "mysocket.h"

MySocket::MySocket(QObject* parent, QTcpSocket* socket)
	: QTcpSocket(parent), _socket(socket)
{
	connect(_socket, &QTcpSocket::stateChanged, [=] {
		if (_socket->state() == QAbstractSocket::UnconnectedState) {
			qDebug() << "Unconnected." << endl;
		}
	});

	connect(_socket, &QTcpSocket::readyRead, [=] {
		QByteArray data = _socket->readAll();

		// ��Ӧ
		//QByteArray ack = QByteArray::fromHex("0101000000080110000100015009");
		char c = data[0];
		if (c == 0x11) {
			QByteArray ack = QByteArray::fromHex("1110000100015009");
			_socket->write(ack);
		}
		qDebug() << QString(data.toHex()) << endl;
	});

	mqtt = new MyMqtt(this);
}

MySocket::~MySocket()
{
}

