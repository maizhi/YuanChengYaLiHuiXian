#pragma once

#include <QObject>
#include <QTcpSocket>

class Sensor : public QObject
{
	Q_OBJECT

public:
	Sensor(QObject* parent);
	~Sensor();

	void test();

private:
	QTcpSocket* socket = nullptr;

signals:
	void data_read(QByteArray);
signals:
	void connected(QString);

public slots:
	void displayError(QAbstractSocket::SocketError );
};
