#include "sensor.h"
#include <QDebug>

Sensor::Sensor(QObject* parent)
	: QObject(parent)
{
	socket = new QTcpSocket(this);

	QString server = "192.168.1.221";
	socket->connectToHost(server, 9001);

	// 关联
	// 错误
	connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &Sensor::displayError);
	// 数据可读
	//connect(socket, &QIODevice::readyRead, this, &MyTcpClient::m_parse);
	connect(socket, &QIODevice::readyRead, [=]() {
		QByteArray data = socket->readAll();
		// 发射数据
		emit(data_read(data));

		// 响应
		QByteArray data_2 = data.mid(12);

		QByteArray ack = QByteArray::fromHex("0101000000080110000100010950");
		socket->write(ack);
	});
	// 连接上
	connect(socket, &QTcpSocket::connected, [=]() {
		emit(connected(QStringLiteral("已连接：%1，端口：%2").arg(server).arg(8899)));
		qDebug() << "connected" << endl;

	});
}

Sensor::~Sensor()
{
}

/* 测试 */
void Sensor::test()
{
	if (socket == nullptr) return;
	QByteArray ack = QByteArray::fromHex("0101000000080110000100010950");
	socket->write(ack);
}


/* 显示错误 */
void Sensor::displayError(QAbstractSocket::SocketError socketError) {
	QString msg = "";
	switch (socketError) {
	case QAbstractSocket::RemoteHostClosedError:
		break;
	case QAbstractSocket::HostNotFoundError:
		msg = tr("The host was not found. ");
		break;
	case QAbstractSocket::ConnectionRefusedError:
		msg = tr("The connection was refused by the peer. ");
		break;
	case QAbstractSocket::SocketAddressNotAvailableError:
		msg = QStringLiteral("socket 地址不可用");
		break;
	default:
		msg = tr("The following error occurred: %1.").arg(socket->errorString());
	}
	qDebug() << msg;

}